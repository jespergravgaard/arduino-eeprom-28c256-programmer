#include <Arduino.h>

/*
 * An implementation of Ben Eaters 28CXXX EEPROM programmer for Arduino
 * Based on https://github.com/beneater/eeprom-programmer
 * See https://www.youtube.com/watch?v=K88pgWhEb1M&feature=youtu.be
 */

/* Initialize the EEPROM programmer by setting up I/O pins */
void initEEPROM();
/* Prepare for reading data from the EEPROM */
void prepareReadEEPROM();
/* Prepare for writing data to the EEPROM */
void prepareWriteEEPROM();
/** Read a byte from an address in the EEPROM. */
byte readEEPROM(unsigned int address);
/** Write a byte to an address in the EEPROM. */
void writeEEPROM(int address, byte data);
/* Removes any write protection from the 28C512 (NOT WORKING!) */
void unprotectEEPROM();
