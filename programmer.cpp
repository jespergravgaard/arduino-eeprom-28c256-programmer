#include <Arduino.h>
#include "eeprom.hpp"
#include "wavgat.hpp"

/* Write progress to the serial output. */
void serialProgress(int address) {
    if (address % 4096 == 0) {
      Serial.println();
    }
    if (address % 64 == 0) {
      Serial.print(".");
    }  
}

/* Program the EEPROM with a ROM. */
void program(unsigned int address, unsigned int size, byte* rom) {
  Serial.print("Programming EEPROM");
  Serial.print(" ADDRESS: $");
  Serial.print(address,HEX);
  Serial.print(" SIZE: $");
  Serial.print(size,HEX);
  Serial.println();
  prepareWriteEEPROM();
  for (unsigned int offset = 0; offset<size; offset++) {
    byte k = pgm_read_byte(rom+offset) ;  
    writeEEPROM(address+offset, k);
    serialProgress(offset);
  } 
  Serial.println();
  Serial.println("SUCCESS! Programming Complete");
}

/* Verify that the contents of the EEPROM matches a ROM. */
void verify(unsigned int address, unsigned int size, byte* rom) {
  Serial.print("Verifying EEPROM");
  Serial.print(" ADDRESS: $");
  Serial.print(address,HEX);
  Serial.print(" SIZE: $");
  Serial.print(size,HEX);
  Serial.println();
  prepareReadEEPROM();
  boolean success = true;
  for (int offset = 0; offset<size; offset++) {    
    byte k = pgm_read_byte(rom+offset) ;  
    byte b = readEEPROM(address+offset);
    if( b != k) {
      // Verification failed
      Serial.println();
      Serial.print("FAILURE! Verification failed at offset $");
      Serial.print(offset,HEX);
      Serial.print(" Expected: $");
      Serial.print(k,HEX);
      Serial.print(" Read: $");
      Serial.print(b,HEX);
      Serial.println();
      success = false;
      break;
    }
    serialProgress(offset);
  } 
  if(success) {
    Serial.println();
    Serial.println("SUCCESS! Verified EEPROM!");
  }
}

/* Erase the EEPROM by programming it full of a single byte. */
void fill(unsigned int address, unsigned int size, byte fill) {
  Serial.print("Filling EEPROM");
  Serial.print(" DATA: $");
  Serial.print(fill,HEX);
  Serial.print(" ADDRESS: $");
  Serial.print(address,HEX);
  Serial.print(" SIZE: $");
  Serial.print(size,HEX);
  Serial.println();
  for (int offset=0; offset<size; offset++) {
    writeEEPROM(address+offset, fill);
    serialProgress(offset);
  }
  Serial.println("Done");  
}

/* Removes any write protection from the 28C512 (NOT WORKING!) */
void unprotect() {
  // Unprotect the EEPROM 
  Serial.println("Unprotecting EEPROM");
  unprotectEEPROM();
  Serial.println("Done");  
}


// Used for hexByte() and hexWord()
char hexBuf[7];

/* Convert a byte to C hexadecimal format */
char* hexByte(byte b) {
    sprintf(hexBuf, " 0x%02x", b);
    return hexBuf;  
}

/* Convert a word to C hexadecimal format */
char* hexWord(unsigned int w) {
    sprintf(hexBuf, "%04x: ", w);
    return hexBuf;  
}

/*
 * Print the contents of the EEPROM
 * Starts at address and prints size bytes.
 */
void show(unsigned int address, unsigned int size) {
  Serial.print("Showing EEPROM");  
  Serial.print(" ADDRESS: $");
  Serial.print(address,HEX);
  Serial.println();
  int line = 0;
  unsigned int offset = 0;
  while(offset<size) {
    if(line==0) {
      Serial.print(hexWord(address+offset));        
    }
    byte readData = readEEPROM(address+offset);
    Serial.print(hexByte(readData));  
    offset++;
    line++;
    if(line==8) {
      Serial.print(" ");        
    }
    if(line==16) {
      Serial.println();
      line=0;
    }
  }
  if(line!=0) {
      Serial.println();    
  }  
}

void read(unsigned int address, unsigned int size) {
  Serial.print("Reading EEPROM (in ROM C-format)");  
  Serial.print(" ADDRESS: $");
  Serial.print(address,HEX);
  Serial.print(" SIZE: $");
  Serial.print(size,HEX);
  Serial.println();
  Serial.println("#include <avr/pgmspace.h>");
  Serial.println("const char ROM_NAME[] = \"Unnamed\";");
  Serial.print("const int ROM_SIZE = ");
  Serial.print(size);
  Serial.println(";");
  Serial.println("const unsigned char ROM[] PROGMEM = {");
  int line = 0;
  int offset = 0;
  while(offset<size) {
    byte readData = readEEPROM(address+offset);
    Serial.print(hexByte(readData));  
    if(offset<size-1) {
      Serial.print(",");
    }
    offset++;
    line++;
    if(line==32) {
      Serial.println();
      line=0;
    }
  }
  if(line!=0) {
      Serial.println();    
  }  
  Serial.println("};");
  Serial.println();  
}
