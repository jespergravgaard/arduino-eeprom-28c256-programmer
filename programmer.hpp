
/* Program the EEPROM with a ROM. */
void program(unsigned int address, unsigned int size, byte* rom);

/* Verify that the contents of the EEPROM matches a ROM. */
void verify(unsigned int address, unsigned int size, byte* rom);

/* Fill the EEPROM with a single value. */
void fill(unsigned int address, unsigned int size, byte fill);

/* Removes any write protection from the 28C512 (NOT WORKING!) */
void unprotect();

/** Show a bit of the EEPROM to Serial. */
void show(unsigned int address, unsigned int size);

/** Read the entire contents of the EEPROM to Serial in C-format.. */
void read(unsigned int address, unsigned int size);
