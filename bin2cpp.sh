if [ $# -ne 1 ];
then 
	echo "usage: $(basename $0) bin_file"
    echo "Converts a ROM bin-file to CPP-format usable by the Arduino EEPROM 28C256 Programmer"
    exit 1
 elif ! [ -e $1 ];
 then
 	echo "File not found $1"
    exit 1   
 else   
	export binfile=$1
	export cppfile=rom-$(basename $1 .bin).cpp
	export name=$(basename $1 .bin)
	export size=$(cat $1 | wc -c)
	echo "Converting ROM BIN-file $binfile to CPP-file $cppfile"
	rm -f $cppfile
	echo "// $name" >> $cppfile
	echo "// Converted from $(basename $binfile) on $(date +%Y-%m-%d)" >> $cppfile
	echo "#include <avr/pgmspace.h>" >> $cppfile
	echo "const char ROM_NAME[] = \"$name\";" >> $cppfile
	echo "const int ROM_SIZE = $size;" >> $cppfile
	echo "const unsigned char ROM[] PROGMEM = {" >> $cppfile
	hexdump -v -e '/1 "0x"' -e '/1 "%02x, "' $binfile  | sed 's/, *$//' | fold -w 120 -s >> $cppfile
	echo "};" >> $cppfile
fi	