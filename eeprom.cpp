#include "eeprom.hpp"
#include "wavgat.hpp"

/*
 * An implementation of Ben Eaters 28CXXX EEPROM programmer for Arduino
 * Based on https://github.com/beneater/eeprom-programmer
 * See https://www.youtube.com/watch?v=K88pgWhEb1M&feature=youtu.be
 */

// Pins used for the EEPROM programmer
int SHIFT_CLK   = A0;
int SHIFT_LATCH = A1;
int SHIFT_DATA  = A2;
int WRITE_EN    = A3;
int EEPROM_D0   = 2;
int EEPROM_D7   = 9;

/*
 * Initialize the EEPROM programmer by setting up I/O pins
 */
void initEEPROM() {
    // Do not write to EPROM initially
  digitalWrite(WRITE_EN, HIGH);
  pinMode2(WRITE_EN, OUTPUT);
  // Prepare shift registers
  pinMode2(SHIFT_DATA, OUTPUT);
  pinMode2(SHIFT_CLK, OUTPUT);
  pinMode2(SHIFT_LATCH, OUTPUT);
  digitalWrite(SHIFT_CLK, LOW);
}

/*
 * Output the address bits and outputEnable signal using shift registers.
 */
void setAddress(int address, bool outputEnable) {
  shiftOut(SHIFT_DATA, SHIFT_CLK, MSBFIRST, (address >> 8) | (outputEnable ? 0x00 : 0x80));
  shiftOut(SHIFT_DATA, SHIFT_CLK, MSBFIRST, address);

  digitalWrite(SHIFT_LATCH, LOW);
  digitalWrite(SHIFT_LATCH, HIGH);
  digitalWrite(SHIFT_LATCH, LOW);
}

/**
 * Set data pins to input
 */
void prepareReadEEPROM() {
  for (int pin = EEPROM_D0; pin <= EEPROM_D7; pin += 1) {
    pinMode2(pin, INPUT);
  }  
}


/**
 * Set data pins to output
 */
void prepareWriteEEPROM() {
  for (int pin = EEPROM_D0; pin <= EEPROM_D7; pin += 1) {
    pinMode2(pin, OUTPUT);
  }  
}

/**
 * Write a byte to the data pins (D0-D7)
 */
void writeData(byte data) {
  for (int pin = EEPROM_D0; pin <= EEPROM_D7; pin += 1) {
    digitalWrite(pin, data & 1);
    data = data >> 1;
  }
}



/*
 * Read a byte from the EEPROM at the specified address.
 */
byte readEEPROM(unsigned int address) {
  setAddress(address, /*outputEnable*/ true);
  byte data = 0;
  for (int pin = EEPROM_D7; pin >= EEPROM_D0; pin -= 1) {
    int readData = digitalRead(pin);
    data = (data << 1) + readData;
  }
  return data;
}

/*
 * Write a byte to the EEPROM at the specified address.
 */
void writeEEPROM(int address, byte data) {
  setAddress(address, /*outputEnable*/ false);
  writeData(data);
  digitalWrite(WRITE_EN, LOW);
  delayMicroseconds(1);
  digitalWrite(WRITE_EN, HIGH);
  delay(10);
}

/**
 * Removes any write protection from the 28C512
 * See http://ww1.microchip.com/downloads/en/DeviceDoc/doc0006.pdf
 * 
 * NOTE: SO FAR THIS HAS NOT BEEN DEMONSTRATED AS WORKING!
 */
void unprotectEEPROM() {
  prepareWriteEEPROM();
  // Ensure OE is low
  setAddress(0x0000, /*outputEnable*/ false);

  setAddress(0x5555, /*outputEnable*/ false);
  digitalWrite(WRITE_EN, LOW);
  writeData(0xaa);
  delayMicroseconds(1);
  digitalWrite(WRITE_EN, HIGH);
  
  setAddress(0x2aaa, /*outputEnable*/ false);
  digitalWrite(WRITE_EN, LOW);
  writeData(0x55);
  delayMicroseconds(1);
  digitalWrite(WRITE_EN, HIGH);

  setAddress(0x5555, /*outputEnable*/ false);
  digitalWrite(WRITE_EN, LOW);
  writeData(0x80);
  delayMicroseconds(1);
  digitalWrite(WRITE_EN, HIGH);

  setAddress(0x5555, /*outputEnable*/ false);
  digitalWrite(WRITE_EN, LOW);
  writeData(0xaa);
  delayMicroseconds(1);
  digitalWrite(WRITE_EN, HIGH);  

  setAddress(0x2aaa, /*outputEnable*/ false);
  digitalWrite(WRITE_EN, LOW);
  writeData(0x55);
  delayMicroseconds(1);
  digitalWrite(WRITE_EN, HIGH);  

  setAddress(0x5555, /*outputEnable*/ false);
  digitalWrite(WRITE_EN, LOW);
  writeData(0x20);
  delayMicroseconds(1);
  digitalWrite(WRITE_EN, HIGH);    

  /*
  writeEEPROM(0x5555,0xaa);
  writeEEPROM(0x2aaa,0x55);
  writeEEPROM(0x5555,0x80);
  writeEEPROM(0x5555,0xaa);
  writeEEPROM(0x2aaa,0x55);
  writeEEPROM(0x5555,0x20);  
  */
  
}
