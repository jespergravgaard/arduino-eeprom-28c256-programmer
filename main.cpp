#include <Arduino.h>
#include "eeprom.hpp"
#include "programmer.hpp"
#include "MemoryFree.h"

// ROMs can be converted from BIN-format to CPP using the bash-script bin2cpp.sh. 
// ROMs can also be read from an existing ROM-chip by using the "r - read" command of the programmer. 

// Include the ROM to verify/program/...

// COMMODORE 64 ROMS
//#include "rom-c64-basic.901226-01.cpp" // C64 Basic
//#include "rom-c64-kernal.901227-03.cpp" // C64 Kernal
//#include "rom-c64-characters.901225-01.cpp" // C64 Characters

// COMMODORE 128 ROMS
//#include "rom-c128-c64.251913-01.cpp" // C128 C64 Basic & Kernal
#include "rom-c128-kernal.318020-03.cpp" // C128 Kernal

// COMMODORE 16/116/Plus/4 ROMS
//#include "rom-plus4-kernal.318004-05.cpp" // C16 / Plus/4 Kernal
//#include "rom-plus4-basic.318006-01.cpp" // C16 / Plus/4 Basic
//#include "rom-plus4-3-plus-1.317053-01.cpp" // Plus/4 3-plus-1 ROM 1
//#include "rom-plus4-3-plus-1.317054-01.cpp" // Plus/4 3-plus-1 ROM 1

// DIAGNOSTIC CARTRIDGES
//#include "rom-cart-c64-deadtest.cpp" // Cartridge: C64 Dead Test  
//#include "rom-cart-c64-diag-586220.cpp" // Cartridge: C64 Diagnostic 586220
//#include "rom-cart-c128-diag-588121.cpp" // Cartridge: C127/40 Diagnostic 588121

void menu();

// The number of supported EEPROM types
int EepromTypeCount = 2;
 
// The supported EEPROM types
enum EepromType {
  AT28C64=0,
  AT28C256=1
};

// The names of the supported EEPROM types
String EepromTypeNames[2] = { "AT28C64","AT28C256" }; 

// The byte sizes of the supported EEPROM types
unsigned int EepromTypeSizes[2] = { 8192, 32768 }; 

// The programming size
unsigned int progSize;
// The EEPROM bank to program into (0-N)
int progBank;

// The EEPROM type
int eepromType = AT28C256;

/* Get the start address of the current bank (progBank). */ 
unsigned int currentBankStart() {
  return progBank * progSize;
}

/* Get the number of banks (given current programming size and eeprom size). */ 
int getBankCount() {
  return (int) ( EepromTypeSizes[eepromType] /  progSize );
}


void setup() {
  Serial.begin(57600);
  Serial.println();
  Serial.println("Initializing Programmer");
  // Initialize the EEPROM Programmer
  initEEPROM();
  progSize = ROM_SIZE;
  menu();

}

void loop() {  
  byte* romBytes = (byte*)&ROM;
  if(Serial.available()){
        String command = Serial.readStringUntil('\n');    
        Serial.println();    
        
        if(command.equals("v")) {
          if(progSize>ROM_SIZE) {
            Serial.println("FAILURE! Programming size larger than ROM size.");            
          } else {
            verify(currentBankStart(), progSize, romBytes);
          }
        } else if(command.equals("p")) {
          if(progSize>ROM_SIZE) {
            Serial.println("FAILURE! Programming size larger than ROM size.");            
          } else {
            program(currentBankStart(), progSize, romBytes);
            verify(currentBankStart(), progSize, romBytes); 
          }    
        } else if(command.equals("t")) {
          program(currentBankStart(), 256, romBytes);
          verify(currentBankStart(), 256, romBytes);          
        } else if(command.equals("s")) {
          show(currentBankStart(), 64);         
        } else if(command.equals("r")) {
          read(currentBankStart(), progSize);       
        } else if(command.equals("e")) {
          fill(currentBankStart(), progSize, 0x00);         
        } else if(command.equals("f")) {
          fill(currentBankStart(), progSize, 0xff);         
        } else if(command.equals("u")) {
          unprotect();          
        } else if(command.equals("+")) {
          progSize *=2;
          if(progSize==0) {
            progSize = EepromTypeSizes[eepromType];            
          }          
          if(progSize>EepromTypeSizes[eepromType]) {
            progSize = EepromTypeSizes[eepromType];
          }
          if(progBank>=getBankCount()) {
            progBank= 0;
          }
        } else if(command.equals("-")) {
          progSize /=2;
        } else if(command.equals("b")) {
          if(++progBank>=getBankCount()) {
            progBank= 0;
          }
        } else if(command.equals("c")) {
          if(++eepromType==EepromTypeCount) {
            eepromType= 0;
          }
          if(progSize>EepromTypeSizes[eepromType]) {
            progSize = EepromTypeSizes[eepromType];
          }          
          if(progBank>=getBankCount()) {
            progBank= 0;
          }
        } else {
          Serial.print("Unknown command '");                    
          Serial.print(command);                    
          Serial.println("'. Try again.");                    
        }   
        menu();     
  }  
}

/* Print the command menu. */
void menu() {

  Serial.println();

  Serial.print("ROM FILE: '");
  Serial.print(ROM_NAME);
  Serial.print("' SIZE: $");
  Serial.print(String(ROM_SIZE, HEX));
  Serial.println();

  Serial.print("EEPROM TYPE: ");
  Serial.print(EepromTypeNames[eepromType]);
  Serial.print(" SIZE: $");
  Serial.print(String(EepromTypeSizes[eepromType], HEX));
  Serial.println();

  Serial.print("PROGRAMMING");
  Serial.print(" SIZE: $");
  Serial.print(String(progSize, HEX));
  Serial.print(" BANK: ");
  Serial.print(progBank+1);
  Serial.print("/");
  Serial.print(getBankCount());
  Serial.print(" ADDRESS: $");
  Serial.print(String(currentBankStart(), HEX));
  Serial.println("");
  
  Serial.println("Please enter command");

  Serial.println(" s  - Show the first bytes of the EEPROM");
  Serial.println(" p  - Program ROM FILE to EEPROM");
  Serial.println(" v  - Verify EEPROM against ROM FILE");
  Serial.println(" t  - Test if the EEPROM is programmable");
  Serial.println(" c  - Change EEPROM type");
  Serial.println(" b  - Change programming bank");
  Serial.println(" -  - Decrease the programming size");
  Serial.println(" +  - Increase the programming size");
  Serial.println(" r  - Read the EEPROM (in ROM C-format)");
  Serial.println(" e  - Erase the EEPROM (fill with 0x00)");
  Serial.println(" f  - Fill the EEPROM (fill with 0xff)");
  Serial.println(" u  - Remove EEPROM software write protection");  
}
