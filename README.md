# Arduino EEPROM AT28C64/AT28C256 Programmer

An Arduino-based Programmer for AT28C64/AT28C256 EEPROMS. 

It is heavily based on Ben Eaters EEPROM Programmer https://github.com/beneater/eeprom-programmer

On top of this simple functions have been added to allow for reading, programming & verifying EEPROMs from byte arrays included in the program code. 

# Choosing a ROM file

Choosing which ROM file to program/verify is done inside the file `main.cpp` , by including a single CPP-file containing the ROM data.

```java
#include "rom-c64-basic.901226-01.cpp" // Commodore 64 Basic
```

Uploading the ROM to the Programmer is done by Uploading the Arduino Program in the Arduino IDE.

If you have a BIN-file containing a ROM bytes you want to work with, you can use the included bash script `bin2cpp.sh` to convert the BIN-file to a CPP-file in the proper format.

The programmer can also read an entire EEPROM (or ROM) and print it to the Arduino Serial Console in the proper CPP-format. This output can then be copy into a CPP-file for later programming to other EEPROMs.

The CPP-formatted ROM-files have the following structure

```java
#include <avr/pgmspace.h>
const char ROM_NAME[] = "basic.901226-01 - C64 Basic";
const int ROM_SIZE = 8192;
const unsigned char ROM[] PROGMEM = {
  0x94, 0xe3, 0x7b, 0xe3, 0x43, 0x42, 0x4d, 0x42, 0x41, 0x53, ...
  ...
};
```

# User Interface

The Arduino EEPROM Programmer is controlled through the Arduino Serial Console by sending it single character commands. 

These commands allow you quite a lot of flexibility in programming, verifying, erasing, unprotecting and reading EEPROMs.

![Menu](https://gitlab.com/jespergravgaard/arduino-eeprom-28c256-programmer/wikis/uploads/29a67dada28ab6b9ebb489518465fb3b/Screenshot_2019-01-20_at_00.16.06.png)

# Building the Programmer 

The schematic for the programmer is the same as Ben Eaters Programmer, and the basic EEPROM-code has been taken from Ben's project. See https://github.com/beneater/eeprom-programmer

![Schematic](https://raw.githubusercontent.com/beneater/eeprom-programmer/master/schematic.png)

The Programmer itself can be built on a breadboard. Ben Eater has created a great video showing how to build it step-by-step. See https://www.youtube.com/watch?v=K88pgWhEb1M&t=1820s

![Breadboard](https://gitlab.com/jespergravgaard/arduino-eeprom-28c256-programmer/wikis/uploads/38c87597bf54b32b4bb0456993f2748c/Screenshot_2019-01-20_at_00.28.27.png)