#include <Arduino.h>

/* Implementation of pinMode() from standard Arduino Board code.
 * Necessary when using a WAVGAT Nano since their pinMode()-implementation has an error. 
 * Since it is a copy from the standard implementation it should work on most boards.
 */
void pinMode2(uint8_t pin, uint8_t mode);
